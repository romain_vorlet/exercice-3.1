package ihm;

import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.DirectAccessBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class BadgeWalletGUI {

    private static final String RESOURCES_PATH = "src/resources/";
    private static final File walletDatabase = new File(RESOURCES_PATH+ "wallet.csv");

    private JButton button1;
    private JPanel panelMain;
    private JTable table1 ;


    public BadgeWalletGUI() {
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Bien joué!");
            }
        });

        DirectAccessBadgeWalletDAO dao = null;

        try {
            //Instanciation de la BDD pour lecture/écriture
            dao = new DirectAccessBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet_indexed.csv");
            System.out.print(RESOURCES_PATH);
            //Création du Metaset
            Set<DigitalBadge> metaSet = dao.getWalletMetadata();
            List<DigitalBadge> tableList = new ArrayList<>();

            // Récupération de la liste des badges à partir des métadonnées
            tableList.addAll(metaSet);

            //Création du TableModel pour gérer de façon plus élégante l'affichage
            TableModel tableModel = TableModelCreator.createTableModel(DigitalBadge.class, tableList);

            //Chargement de l'affichage dans le container d'affichage de type JTable
            table1.setModel(tableModel);

        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        button1.addActionListener(new ActionListener() {
            /**
             * Invoked when an action occurs.
             *
             * @param e the event to be processed
             */
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });
    }

    public static void main(String[] args) {
        JFrame Frame = new JFrame("badges");

        Frame.setContentPane(new BadgeWalletGUI().panelMain);

        Frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Frame.pack();
        Frame.setVisible(true);
    }
}
