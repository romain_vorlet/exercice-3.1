package ihm;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;

public class BadgeSizeCellRenderer  extends DefaultTableCellRenderer {

    Color originBackground;
    Color originForeground;

    public BadgeSizeCellRenderer() {

    }

    //
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        Byte tailleOctets = (Byte) value ;
        String temp = tailleOctets.toString();
        setText(tailleOctets.toString());

        if (tailleOctets > 10) {
            setBackground(Color.ORANGE);
        }

        return this;
    }


    private String humanReadableByteCountBin(long bytes) {
        long absB = bytes == Long.MIN_VALUE ? Long.MAX_VALUE : Math.abs(bytes);
        if (absB < 1024) {
            return bytes + " o";
        }
        long value = absB;
        CharacterIterator ci = new StringCharacterIterator("KMGTPE");
        for (int i = 40; i >= 0 && absB > 0xfffccccccccccccL >> i; i -= 10) {
            value >>= 10;
            ci.next();
        }
        value *= Long.signum(bytes);
        return String.format("%.1f %co", value / 1024.0, ci.current());
    }

}
