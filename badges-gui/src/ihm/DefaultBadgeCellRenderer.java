package ihm;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

public class DefaultBadgeCellRenderer extends DefaultTableCellRenderer {

    Color originForeground ;

    // Constructeur
    public DefaultBadgeCellRenderer () {

    }

    // Méthode permettant l'affichage en rouge de la date de fin lorsque cette dernière est dépassée
    // Ce sont les éléments de la ligne i des colonnes 1 à 4 qui doivent être colorées si la date de fin < date du jour
    // il faut donc récupérer la valeur de la variable end, comparer avec la date du jour et appliquer le traitement aux autres attributs de la ligne
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        Date end = (Date) value;
        setText(end.toString());
        Date aujourdhui = new Date();
        long lon = aujourdhui.getTime();
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date result = new Date(lon);


        if (end.compareTo(result)>= 0) {
            setForeground(Color.RED);
        }

        return this;
    }
}
