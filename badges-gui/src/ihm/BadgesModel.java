package ihm;

import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;

import javax.swing.table.AbstractTableModel;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;

public class BadgesModel extends AbstractTableModel {

    private List<DigitalBadge> badges ;
    String []entetes = {"ID", "Code Série", "Début", "Fin", "Tailles (octets)"} ;
    private static final long serialVersionUID = 99995555L ;

    public BadgesModel () {
        super ();

           }

     // Retourne les badges de la BDD sous forme de liste
     public List<DigitalBadge> getBadges () {
        return badges;
     }

    // Renvoi l'intitulé de colonne
    @Override
    public String getColumnName(int columnIndex) {

        return entetes[columnIndex];
    }
    /**
     * Returns the number of rows in the model. A
     * <code>JTable</code> uses this method to determine how many rows it
     * should display.  This method should be quick, as it
     * is called frequently during rendering.
     *
     * @return the number of rows in the model
     * @see #getColumnCount
     */

    // Renvoi la taille de la liste badges, soit le nombre de lignes du tableau
    @Override
    public int getRowCount() {

        return badges.size();
    }

    /**
     * Returns the number of columns in the model. A
     * <code>JTable</code> uses this method to determine how many columns it
     * should create and display by default.
     *
     * @return the number of columns in the model
     * @see #getRowCount
     */
    // Retourne le nombre de colonne du tableau
    @Override
    public int getColumnCount() {

        return entetes.length;
    }

    /**
     * Returns the value for the cell at <code>columnIndex</code> and
     * <code>rowIndex</code>.
     *
     * @param rowIndex    the row whose value is to be queried
     * @param columnIndex the column whose value is to be queried
     * @return the value Object at the specified cell
     */
    // Retourne le contenu d'une cellule selon l'indice de ligne et de colonne
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {

            case 0 :
                return badges.get(rowIndex).getMetadata().getBadgeId();

            case 1:
                return badges.get(rowIndex).getSerial();

            case 2:
                return badges.get(rowIndex).getBegin();

            case 3:
                return badges.get(rowIndex).getEnd();

            case 4:
                return badges.get(rowIndex).getSerial().getBytes(StandardCharsets.UTF_8);
        }
        return null;
    }

    // Marqueur pour la modification graphique des cellules pour la back ou foreground
    public Class<?> getColumnClass (int columnIndex) {
        switch (columnIndex) {

            case 0:
                return Integer.class;

            case 1:
            case 4:
                return String.class;

            case 2:
            case 3:
                return Date.class;

        }
        return null;
    }
}
